# Getting started with Slack [#](https://slack.com/intl/en-nl/help/articles/206845317-Create-a-Slack-workspace)

If you haven't got a Slack workspace yet, create one:

1) Go to [https://slack.com/get-started#/createnew](https://slack.com/get-started#/createnew).
2) Enter your email address and click Continue, or continue with Apple or Google.
3) Check your email for a confirmation code.
4) Enter your code, then click Create a Workspace and follow the prompts.

New to being a Slack Workspace admin? Go to [Getting started for workspace creators](https://slack.com/intl/en-nl/help/articles/217626298-Getting-started-for-workspace-creators)
for more information on how to get started.

## Create a bot for your workspace [#](https://slack.com/intl/en-nl/help/articles/115005265703-Create-a-bot-for-your-workspace)

A bot is a nifty way to run code and automate messages and tasks. 
In Slack, a bot is controlled programmatically via a bot user token that can access one or more of Slack’s APIs. 

### What can bots do?

* 🤖 Monitor and help process channel activity
* 🤖 Post automated messages in Slack
* 🤖 Make channel messages interactive with buttons

## Add a bot user

1) Initialize the bot _(<1 minute)_
2) Configure your bot _(depends)_
3) Install your bot _(<1 minute)_

### Add a bot - 1: Initialize the bot

This shouldn't be too hard, and will take less than 1 minute.

1) [Create a Slack app](https://api.slack.com/apps/new) if you don't already have one, or select an existing app you've created.
2) Choose to _"create an app from an app manifest"_
3) Pick a development workspace and click **Next**
4) Just click **next** on the _"Enter app manifest below"_ screen (you can configure this later)
5) On the next screen (_"Review summary & create your app"_), click **create**

### Add a bot - 2: Configure your bot

Configure your app (_left sidebar: Settings > **App Manifest**_) ([docs](https://api.slack.com/reference/manifests))

<details>
<summary>Example manifest:</summary>

```
{
    "_metadata": {
        "major_version": 1,
        "minor_version": 1
    },
    "display_information": {
        "name": "My bot",
        "long_description": "A long description for my bot that is at least 175 characters long... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ..",
        "description": "A short description for my bot",
        "background_color": "#003300"
    },
    "features": {
        "bot_user": {
            "display_name": "My bot",
            "always_online": true
        }
    },
    "oauth_config": {
        "scopes": {
            "bot": [
                "incoming-webhook",
                "commands",
                "app_mentions:read",
                "im:read",
                "im:write",                
                "mpim:read",
                "mpim:write",
                "chat:write",
                "chat:write.public"
            ]
        }
    },
    "settings": {
        "org_deploy_enabled": false,
        "socket_mode_enabled": false,
        "token_rotation_enabled": false
    }
}
```
</details>

For a list of scopes, see [here](https://api.slack.com/scopes)

### Add a bot - 3: Install your bot

Install the app in your workspace (_left sidebar: Distribution > **Install App**_)

Go to 
* Manage Distribution
* open "Enable Features & Functionality"
* Click "Bots"
* Now you are in "App Home". Find and check the option: 
> "Allow users to send Slash commands and messages from the messages tab"

### notes
How do I find a channel's ID if I only have its #name?
> Use `conversations.list` to retrieve a list of channels. The list includes each channel's name and id fields.
