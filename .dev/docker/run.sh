#!/usr/bin/env bash

# Prepare Project
chmod 777 -R storage vendor bootstrap/cache/

if [ ! -f /var/run/crond.pid ]; then
    echo "Starting the cron"
    service cron start
fi

if pidof service cron status >/dev/null
then
    echo "Started the cron successfully"
else
    echo "Cron not running (re)starting"
    service cron restart
    echo service cron status
fi

# Reload configuration
php artisan config:clear

# Start apache
source /etc/apache2/envvars
if [ ! -f /var/run/apache2/apache2.pid ]; then
    echo "Starting Apache"
    exec apache2 -D FOREGROUND
fi

# Start supervisor to keep the queues running
service supervisor start

# keep this container running
while [ -f /var/run/supervisord.pid ]; do
    sleep 1
done
