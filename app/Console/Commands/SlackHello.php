<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Notifications\SayHello;
use Illuminate\Console\Command;

class SlackHello extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:hello';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validates the certificates of a batch of domains';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $notification = new SayHello();
        $user = User::make([
            'name'           => 'Test User',
            'email'          => 'test@example.com',
            'slack_username' => 'steef',
        ]);

        $user->notify($notification);

        return 0;
    }
}
