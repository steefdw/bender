<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'slack_username',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Route notifications for the Slack channel.
     *
     * @param  Notification  $notification
     *
     * @return string
     */
    public function routeNotificationForSlack(Notification $notification): string
    {
        return env('SLACK_STATUS_NOTIFICATION_WEBHOOK');
    }

    public function getSlackChannelAttribute(): string
    {
        if ($this->slack_username) {
            return '@' . $this->slack_username;
        }

        return '#status';
    }
}
