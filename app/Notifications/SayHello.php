<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class SayHello extends Notification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  User $notifiable
     * @return array<string>
     */
    public function via(User $notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  User $notifiable
     * @return SlackMessage
     */
    public function toSlack(User $notifiable): SlackMessage
    {
        $message = 'This a message from the Slackbot';
        dump('Sending "' . $message . '" to ' . $notifiable->slackChannel);

        return (new SlackMessage())
            ->error()
            ->from('Slackbot', ':ghost:')
            ->to($notifiable->slackChannel)
            ->content($message)
            ->attachment(function ($attachment) use ($notifiable) {
                $attachment
                    ->title('Hello world')
                    ->fields([
                        'name'  => $notifiable->name,
                        'email' => $notifiable->email,
                    ]);
            });
    }
}
