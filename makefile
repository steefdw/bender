container=slackbot

include ./.dev/makefile

.PHONY: help welcome build up down

# Checks if the current process is inside a docker container or not
ifeq ("$(shell test -f /.dockerenv && test "$$CI" != "true" && echo $$?)", "0")
    CONTAINER_COMMAND =
else
    CONTAINER_COMMAND = docker exec -it --workdir "/var/www/html" app-$(container)
endif

## Build the docker containers
build: inside-container-check
	docker-compose rm -vsf
	docker-compose down -v --remove-orphans
	docker volume create --name=db-healthchecker
	docker-compose build
	docker-compose up -d

## Docker-compose up (with check that traefik is running)
up: inside-container-check
	docker start traefik2 | echo 'already running'
	docker-compose up -d

## Docker-compose down
down: inside-container-check
	docker-compose down

## Run `composer dumpauto` inside the container
autoload:
	@${CONTAINER_COMMAND} composer dumpauto

## Run `composer install` inside the container
install:
	@${CONTAINER_COMMAND} composer install

## Run `composer require` inside the container
require:
	@${CONTAINER_COMMAND} composer require

## Run `composer require-dev` inside the container
require-dev:
	@${CONTAINER_COMMAND} composer require --dev

## Jump inside the container
jumpin: inside-container-check
	docker exec -it app-$(container) bash

## Clear all application cache
clear:
	@${CONTAINER_COMMAND} php artisan optimize:clear
	@${CONTAINER_COMMAND} php artisan view:clear
	@${CONTAINER_COMMAND} php artisan route:clear
	@${CONTAINER_COMMAND} php artisan cache:clear

## Run `phpunit` tests inside the container
test:
	@${CONTAINER_COMMAND} php ./vendor/bin/phpunit -c phpunit.xml

## Run `phpstan` inside the container
stan:
	@${CONTAINER_COMMAND} php ./vendor/bin/phpstan analyse --level 8 --memory-limit=1024M

## Tail the docker logs from this application
tail-logs: inside-container-check
	docker-compose logs -f $(container)
