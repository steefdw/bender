# Slackbot

## Onboarding

1) Follow the steps for the [traefik-workspace](https://gitlab.com/steefdw/traefik-workspace)
2) clone this repo
3) run `make build`
4) run `make install`
5) done!

Now you can go to [https://slackbot.docker.test/](https://slackbot.docker.test/) and see everything is working.
Not working? Check `make tail-logs` for errors
